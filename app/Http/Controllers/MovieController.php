<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Movie;
use App\Models\Moviecategory;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class MovieController extends Controller
{
    public function getMovie()
    {
        $movie = Movie::get();
        $movie->loadMissing('category');
        return $movie;
    }

    public function saveMovie(Request $request)
    {
        $imagePath = null;
        if($request->file('image'))
        {
            $imagePath= $request->file('image')->store('MovieImages', 'public');
            $image = Image::make(public_path("storage/{$imagePath}"))->fit(1200, 1600);
            $image->save();
        }

        $movieId = Movie::create(['title' => $request->title, 'year' => $request->year, 'description' => $request->description, 'image' => $imagePath])->getKey();
        if($request->category != null)
        {
            $category =  explode(',', $request->category);
            foreach($category as $categoryId)
            {   
                Moviecategory::create(['category_id'=>$categoryId, 'movie_id'=>$movieId]);
            }
        }
        return response()->json(['success' => 'Successfully Added']);
    }

    public function getCategorys()
    {
        $category = Category::get();
        return $category;
    }

    public function saveUser(Request $request)
    {
        $user = User::where('email', $request->json('email'))->first();
        if ($user) {
            return response()->json(['error' => 'User is already taken']);
        }
        $newUser = User::create(['name' => $request->json('name'), 'email' => $request->json('email'), 'password' => Hash::make($request->json('password')), 'admin' => 0]);
        //    $token = $newUser->createToken('Token Name')->accessToken;
        return $newUser;
    }

    public function login(Request $request)
    {   
        $user = User::where('email', $request->json('email'))->first();
        // dd($request['password'], $user);
        if(!$user)
        {
            return response()->json(['error' =>'There is no user in this email']);
        }
        if (!Hash::check($request['password'], $user->password)) {
            return response()->json(['error' =>'Password is Incorrect']);
        }
        return $user;
    }
}
