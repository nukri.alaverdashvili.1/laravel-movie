<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script defer src="{{mix('js/app.js')}}"></script>
    <link defer href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.flowplayer.com/releases/native/stable/style/flowplayer.css">
    <script src="//cdn.flowplayer.com/releases/native/stable/flowplayer.min.js"></script>
    <!-- Optional plugins -->
    <script src="//cdn.flowplayer.com/releases/native/stable/plugins/hls.min.js"></script>
    <title>Movies</title>
</head>
{{-- // style={{backgroundImage:`url(https://images.unsplash.com/photo-1568035012504-963fcc93852c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDE0fHx8ZW58MHx8fHw%3D&w=1000&q=80)`, backgroundSize:'cover', backgroundRepeat:'no-repeat'}} --}}
<body style="background-image: url('https://www.diefeuermacher.de/wp-content/uploads/2019/10/film-background-1334067869u9d.jpg '); background-repeat: no-repeat, repeat; background-size: cover;" >
<div id='main' class='main'> </div>
</body>
</html>

{{-- https://www.diefeuermacher.de/wp-content/uploads/2019/10/film-background-1334067869u9d.jpg --}}