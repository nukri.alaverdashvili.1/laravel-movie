import React, {useState, useEffect} from 'react'
import { Input } from 'react-rainbow-components';
import axios from 'axios'
import Validation from './Validation'
import Background from '../Images/background.png'

function Register() {
    const [name, setName]=useState('')
    const [password, setPassword]=useState('')
    const [password2, setPassword2]=useState('')
    const [email, setEmail]=useState('')
    const [error, setError]=useState(false)
    const [success, setSuccess] = useState(false)

    async function signUp(e) //ფუნქცია გამოიძახება რეგისტრაციის ღილაკს დააჭერენ ხელს
    {
        e.preventDefault();
        let item={name,password, password2, email} //ყველა ცვლადს რომელიც ზემოთ აღვწერეთ ვინახავთ item ში

        let check = Validation(item) //ვუკეთებთ ითემებს ვალიდაციას, რომელიც ცალკე ფუნქციაა
        if(!check) 
        {
            const result = await axios.post('http://127.0.0.1:8000/api/saveuser',{name, password, email})
            if(result.data.msg)
            {
                console.log(result.data.msg)
                setError({msg:result.data.msg})
            }else{
                setSuccess('Successfuly Registrierd Please Logged In')
                window.setTimeout(function(){
                    window.location = '/'
                },1000)
            }
        }else{
            setError(check) //ერორის შემთხვევაში 
        }
    }
    return (
        <div className='container' >
        <div className='mt-5 mb-5 m-auto'>
       {error &&
       <>
       {error.name && <div className="alert alert-danger" role="alert"> {/* თუ ნეიმის ერორია მაშგ შემთხვევაში გამოიტანს მხოლოდ ამ შეტყობინებას */}
           <h5>{error.name}</h5>
       </div>
        }
        {error.password && 
       <div className="alert alert-danger" role="alert">
           <h5>{error.password}</h5>
       </div>
        }
        {error.msg && 
       <div className="alert alert-danger" role="alert">
           <h5>{error.msg}</h5>
       </div>
        }
        {error.email && 
       <div className="alert alert-danger" role="alert">
           <h5>{error.email}</h5>
       </div>
        }
        {error.confirmPassword && 
       <div className="alert alert-danger" role="alert">
           <h5>{error.confirmPassword}</h5>
       </div>
        }
        </>
        }

        {!error && success && <div className="alert alert-success" role="alert">{/* თუ ერორები არ არის და საქსესი თრუ არის მაგ შემთხვევაში გამოიტანს საქსესის მესიჯს */}
                <h5> {success} </h5>
        </div>}
        <h1 style={{maxWidth:'300px', margin:'auto', paddingTop:'20px', color:'white'}}> Registration </h1>
        <form className='m-auto mt-5' onSubmit={signUp} >
            <Input
                placeholder="Name"
                type="text"
                className="rainbow-p-around_medium"
                 style={{maxWidth:'300px', margin:'auto', paddingTop:'20px'}}
                value={name}  //ამ ინფუთის მნიშვნელობა არის ზემოთ აღნიშნული ნეიმი თავიდან ცარიელი სტრინგი
                onChange={(e)=>setName(e.target.value)} // ინფუთში რამის ჩაწერის შემდეგ ნეიმი შეიცვლება და გახდება ის მნიშვნელობა რასაც ინფუთში ჩავწერთ
                />

            <Input
                placeholder="**********"
                type="password"
                className="rainbow-p-around_medium"
                 style={{maxWidth:'300px', margin:'auto', paddingTop:'20px'}}
                value={password} 
                onChange={(e)=>setPassword(e.target.value)}
            />
              <Input
                placeholder="Confirm Password ***** "
                type="password"
                className="rainbow-p-around_medium"
                 style={{maxWidth:'300px', margin:'auto', paddingTop:'20px'}}
                value={password2} 
                onChange={(e)=>setPassword2(e.target.value)}
            />
            <Input
                placeholder="inputEmail@gmail.com"
                type="email"
                className="rainbow-p-around_medium"
                 style={{maxWidth:'300px', margin:'auto', paddingTop:'20px'}}
                value={email} 
                onChange={(e)=>setEmail(e.target.value)}
            />
                <div className="row justify-content-center align-content-center pt-3 pb-3">
                <div className="col-9 offset-6">
                    <input
                        type="submit"
                        className="btn btn-primary align-content-center"
                        style={{marginLeft:'100px'}}
                    />
                    </div>
                </div>
          </form>
        </div>
        </div>
    )
}

export default Register
