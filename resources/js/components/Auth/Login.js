import React, {useState} from 'react'
import { Input } from 'react-rainbow-components';
import axios from 'axios'
import Cookies from 'js-cookie'
import Background from '../Images/background.png'

function Login() {
    const [email, setEmail]=useState('')
    const [password, setPassword]=useState('')
    const [error, setError]=useState(false)

    async function signUp(e)
    {
        e.preventDefault();
        if(email == '')
        {
            setError('Email is Required');
        }else if(password =='')
        {
            setError('Password is Required');
        }else{
            const result = await axios.post('http://127.0.0.1:8000/api/login',{email, password})
            if(result.data.error)
            {
                setError(result.data.error) // თუ სერვერიდან მივიღებთ msg ს მასინ ერორებში შევინახავთ
            }else{  
                Cookies.set('token', result.data.id) //თუ ერორები არ გვაქვს cook ში ვინახავთ სერვერიდან წამოსულ ტოკენს
                console.log(result.data);
                window.location = '/'; //გადაამისამართებს პროფილზე
            }
        }
    }
    return (
        <div className='container' >
        <div className='mt-5 mb-5 m-auto'>
       {error && <div className="alert alert-danger" role="alert">
           <h5>{error}</h5>
       </div>
        }
        <h1 style={{maxWidth:'300px', margin:'auto', paddingTop:'20px', color:'white'}}> Login </h1>
        <form className='m-auto mt-5' onSubmit={signUp} >
            <Input
                placeholder="Input Email"
                type="email"
                className="rainbow-p-around_medium"
                 style={{maxWidth:'300px', margin:'auto', paddingTop:'20px'}}
                value={email} 
                onChange={(e)=>setEmail(e.target.value)}
                />
            <Input
                placeholder="**********"
                type="password"
                className="rainbow-p-around_medium"
                 style={{maxWidth:'300px', margin:'auto', paddingTop:'20px'}}
                value={password} 
                onChange={(e)=>setPassword(e.target.value)}
            />
            <div className="row justify-content-center align-content-center pt-3 pb-3">
            <div className="col-9 offset-6">
              <input
                type="submit"
                className="btn btn-primary align-content-center"
                style={{marginLeft:'100px'}}
              />
            </div>
          </div>

            </form>
        </div>
        </div>
    )
}

export default Login
