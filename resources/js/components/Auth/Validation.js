const Validation =(values)=>{
    let errors = {};

    if(values.name.length <5)
    {
        errors.name = 'Name must be minimum 5 charachter'
    }else if (!values.name)
    {
        errors.name = 'Names is Required'
    }

    if(values.password =='')
    {
        errors.password = 'Password is required'
    }else if (values.password.length <5)
    {
        errors.password = 'Password must be minimum 5 Character'
    }

    if(values.password2 == '')
    {
        errors.confirmPassword = 'Confrim Password is Required';
    }else if(values.password !== values.password2)
    {
        errors.confirmPassword = 'Password doesnt Match';
    }

    if(values.email =='')
    {
        errors.email = 'Email is Required';
    }

    if(errors.password || errors.name || errors.email ||errors.confirmPassword)
    {
        return errors;
    }
    
    return false;
}

export default Validation;