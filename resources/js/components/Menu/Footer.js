import React from 'react';
import {BsFacebook,BsTwitter, BsInstagram, BsSearch} from 'react-icons/bs'

function Footer() {
  return (
    <footer style={{marginTop:'150px'}} className="p-5 text-white text-center">
    <div className="container">
          <a href='https://www.facebook.com/'><BsFacebook className='mr-3' size={'45px'}/></a>
          <a href='https://www.twitter.com/'><BsTwitter className='mr-3' size={'45px'}/></a>
          <a href='https://www.instagram.com/'><BsInstagram className='mr-3' size={'45px'}/></a>
             Copyright && Copy ExamCenter GmbH
    </div>
    </footer>
  )
}

export default Footer;
