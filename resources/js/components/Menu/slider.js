import React from 'react';
import {Carousel} from '3d-react-carousal';


function Slider({movies}) {
    let slide = ()=>{
        let slides = movies.map((movie)=>{
             return <img style={{width:'800px', height:'300px', margin:'auto', backgroundColor:'hsl(0, 0%, 90%)', transition:'background-color 300ms', WebkitUserSelect:'none'}} src={('/storage/'+movie.image)} alt='1' />
         });
        return slides;
     }
  return(
        <Carousel slides={slide()} interval={1000}/>
        //     <div id="carouselExampleInterval" className="carousel slide" data-bs-ride="carousel">
        //     <div className="carousel-inner">
        //         {movies[0] && 
        //       <div className="carousel-item active" data-bs-interval={10000}>
        //             <img style={{width:'800px', height:'300px', margin:'auto', backgroundColor:'hsl(0, 0%, 90%)', transition:'background-color 300ms', WebkitUserSelect:'none'}}  src={('/storage/'+movies[0].image)} className="d-block w-100" alt="..." />
        //         </div>
        //         }
        //         {movies.map((movie)=>{
        //         return (<div className="carousel-item" data-bs-interval={2000}>
        //              <img style={{width:'800px', height:'300px', margin:'auto', backgroundColor:'hsl(0, 0%, 90%)', transition:'background-color 300ms', WebkitUserSelect:'none'}} src={('/storage/'+movie.image)} className="d-block w-100" alt='1' />
        //           </div>
        //         )})
        //         }
        //     </div>
        //     <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="prev">
        //     <span className="carousel-control-prev-icon" aria-hidden="true" />
        //     <span className="visually-hidden">Previous</span>
        //     </button>
        //     <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleInterval" data-bs-slide="next">
        //     <span className="carousel-control-next-icon" aria-hidden="true" />
        //     <span className="visually-hidden">Next</span>
        //     </button>
        // </div>
  ) 

}

export default Slider;
