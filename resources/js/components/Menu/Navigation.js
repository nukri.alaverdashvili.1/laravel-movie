import React, {useState} from 'react'
import { Link } from "react-router-dom";
import Logo from '../Images/51.png';
import {BsSearch} from 'react-icons/bs'
import { useNavigate,  } from "react-router-dom";

function Navigation({loginData, categorys, searchFunction, logOut}) {
    const [search, setSearch] = useState('');
    let history = useNavigate();
    
    async function searchSubmit(e)
    {
        e.preventDefault();
        searchFunction(search);
        setSearch('');
        history('/search');
    }

    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light">
            <div className="container-fluid">
            <Link className="navbar-brand" to="/"><img style={{width:'100px'}} src={Logo}></img></Link>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                    <Link className="nav-link active" aria-current="page" to="/">Home</Link>
                </li>
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" to="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Category
                    </a>
                    <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                        {
                            categorys && categorys.map((category)=>{
                                return (<li key={category.id}><Link className="dropdown-item" to={'/'+category.category_name} >{category.category_name}</Link></li>)
                            })
                        }
                    </ul>
                </li>
                <li className="nav-item">
                    {!loginData && <Link className="nav-link" to="/registration">Registration</Link>}
                </li>
                <li className="nav-item">
                    {loginData ? <Link className="nav-link" to='/' onClick={logOut}>LogOut</Link> : <Link className="nav-link" to="/login">Login</Link>}
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/create">Create</Link>
                </li>
                </ul>
            </div>
            </div>
        </nav>

        {/* Search */}
        <form onSubmit={searchSubmit} className="d-flex mb-5 mt-2">
                <input className="form-control me-2" type="search" value={search} placeholder="Search" onChange={(e)=>setSearch(e.target.value)} />
                <button className="btn btn-outline-success" type="submit"><BsSearch />Search</button>
        </form>
        </div>
    )
}

export default Navigation
