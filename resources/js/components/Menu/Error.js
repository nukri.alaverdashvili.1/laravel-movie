import React from 'react'

function Error() {
    return (
        <div className='container'>
            <h1>Page Doesnt Exist</h1>
        </div>
    )
}

export default Error
