import React, {useState, useEffect} from 'react'
import { Link } from "react-router-dom";
import Slider from './slider'
import Footer from './Footer'
function Main({movies}) {
    return (
        <div>
        <div className='container'>
        {/* Slider */}
        <Slider movies={movies}/>

        {/* Card */}
            <div style={{minHeight:'350px'}} className="row row-cols-1 row-cols-md-3 g-4 mt-5">
            {movies.map((movie)=>{
                return (
                <div key={movie.id} className="col">
                <div className="card h-100">
                    <Link to={{pathname:`/singlemovie/${movie.id}`}}><img key={movie.image} style={{height:'450px'}} src={('/storage/'+movie.image)} className="card-img-top" alt="..." /></Link> 
                    <div className="card-body">
                    <h5 key={movie.title} className="card-title">Title: {movie.title}</h5>
                    <h5 key={movie.year} className="card-title">Year: {movie.year}</h5>
                    <p key={movie.description} className="card-text">Description: {movie.description}</p>
                    <p key={movie.category} className="card-text">Categorys: {
                        movie.category && movie.category.map((cat)=>{
                            return (`| `+cat.category_name+` |`);
                        })
                    }</p>

                    </div>
                 </div>
                 </div>
                )
            })}
            </div>

        </div>
        {/* Footer */}
        <Footer />
        </div>
    )
}

export default Main
