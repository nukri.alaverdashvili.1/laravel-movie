import React, { Component, useState, useEffect } from "react";
import Axios from "axios";
import { Input, CheckboxGroup, Textarea, FileSelector } from "react-rainbow-components";
import { useNavigate,  } from "react-router-dom";
import Validation from './validation';
import Background from '../Images/background2.png'
function Create() {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [image, setImage] = useState(new Date());
    const [year, setyear] = useState('');
    const [files, setFiles] = useState('');
    const [error, setError] = useState(false);
    const [categorys, setCategorys] = useState([]);
    const [category, setCategory] = useState([]);
    let history = useNavigate();

    useEffect( async ()=>{
        await Axios.get('http://127.0.0.1:8000/api/categorys').then((res)=>{
            setCategorys(res.data);
        }).catch((error)=>{
            setError({'msg':'Server error'});
        });
    },[]);
      async function onSubmit(e)
      {
          e.preventDefault();
          const post = {title, year, description};
          const check = Validation(post);
          if(!check)
          {
            const formData = new FormData();
            formData.append('post', post);
            formData.append('image', image);
            formData.append('title', title);
            formData.append('year', year);
            formData.append('description', description);
            formData.append('category', category);
  
          await Axios.post('http://127.0.0.1:8000/api/savemovie', formData,{"title":title, "description": description, 'year':year, 'category':category}).then((res)=>{
            console.log(res);
            if(res.data.success)
            {
              history('/');
            }else{
              setError(res.data.error);
            }
        }).catch((error)=>{
          setError('Server Error');
        })
          }else{
            setError(check);
          }

      }
  
      const handleChange = value => {
        setImage(value[0]);
        console.log(value);
        if(value.length>0)
        {
          setFiles(URL.createObjectURL(value[0]));
        }else{
          setFiles(false);
        }
    }
    // style={{backgroundImage:`url(https://images.unsplash.com/photo-1568035012504-963fcc93852c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDE0fHx8ZW58MHx8fHw%3D&w=1000&q=80)`, backgroundSize:'cover', backgroundRepeat:'no-repeat'}}
    return (
        <div className='container' >
        <div className='mt-5 mb-5 m-auto '>
        {error &&
       <>
       {error.year && <div className="alert alert-danger" role="alert"> {/* თუ ნეიმის ერორია მაშგ შემთხვევაში გამოიტანს მხოლოდ ამ შეტყობინებას */}
           <h5>{error.year}</h5>
       </div>
        }
        {error.title && 
       <div className="alert alert-danger" role="alert">
           <h5>{error.title}</h5>
       </div>
        }
         {error.description && 
       <div className="alert alert-danger" role="alert">
           <h5>{error.description}</h5>
       </div>
        }
         {error.msg && 
       <div className="alert alert-danger" role="alert">
           <h5>{error.msg}</h5>
       </div>
        }
        </>
        }
        <h1 style={{maxWidth:'300px', margin:'auto', paddingTop:'20px', color:'white'}}> Create Post </h1>
        <form className='m-auto mt-5' onSubmit={onSubmit}>
          <Input
            placeholder="Title"
            type="text"
            className="rainbow-p-around_medium"
            style={{maxWidth:'300px', margin:'auto', paddingTop:'20px'}}
            value={title}
            onChange={(e)=>setTitle(e.target.value)}
          />
          {
            categorys && categorys.map((cat)=>{
              return (
                <div key={cat.id} style={{maxWidth:'300px', margin:'auto', paddingTop:'20px'}} className="form-check">
                <input className="form-check-input" type="checkbox" onChange={(e)=>{setCategory([...category, e.target.value])}} name={cat.id} value={cat.id}/>
                <label className="form-check-label" htmlFor="flexCheckDefault">
                  {cat.category_name}
                  </label>
                </div>
              )
            })
          }
            <Textarea
              id="example-textarea-1"
              rows={4}
              placeholder="Movie Description"
              style={{maxWidth:'300px', margin:'auto', paddingTop:'20px'}}
              className="rainbow-m-vertical_x-large rainbow-p-horizontal_medium rainbow-m_auto"
              value={description}
              onChange={(e)=>setDescription(e.target.value)}  
          />
            <Input
                  placeholder="Year"
                  type="number"
                  className="rainbow-p-around_medium"
                  style={{maxWidth:'300px', margin:'auto', paddingTop:'20px'}}
                  onChange={(e)=>setyear(e.target.value)}  
              />
          <FileSelector
                className="rainbow-m-vertical_x-large rainbow-p-horizontal_medium rainbow-m_auto"
                style={{width:'450px', margin:'auto', paddingTop:'20px'}}
                placeholder="Click to Browse Image"
                variant="multiline"
                onChange={handleChange}
          />
          <img style={{ height: "150px", margin:'auto', marginTop:'20px'}} src={files} />
          <div className="row justify-content-center align-content-center pt-3 pb-3">
          <div className="col-9 offset-6">
              <input
                type="submit"
                className="btn btn-primary align-content-center"
                style={{marginLeft:'100px'}}
              />
            </div>
          </div>
        </form>
        </div>
        </div>
    )
}

export default Create
