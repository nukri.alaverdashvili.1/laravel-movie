const Validation =(values)=>{
    
    let errors = {};

   if (values.title == '')
    {
        errors.title = 'Title is Required'
    }

    if(values.year =='')
    {
        errors.year = 'Year is required'
    } 
    
    if (values.description =='')
    {
        errors.description = 'Description is required'
    }

    if(errors.title || errors.year || errors.description)
    {
        return errors;
    }
    return false;
}

export default Validation;