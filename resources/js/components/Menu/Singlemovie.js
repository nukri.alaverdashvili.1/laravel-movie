import React, {useState, useEffect} from 'react'
import { useLocation } from 'react-router-dom'
import flowplayer from '@flowplayer/player'
import Navigation from './Navigation';

function Singlemovie({movies}) {

    const location = useLocation();
    let pathname = location.pathname.slice(13);

    return (
        <div className='container'>
        {/* MovieInformation */}
            {
                movies.map((movie)=>{
                    if(movie.id == pathname)
                    {
                        return (
                        <div key={movie.id} className="row row-cols-1 row-cols-md-2 g-4 mt-5">
                        <div className="col">
                        <div className="card">
                            <img style={{height:'540px'}} src={('/storage/'+movie.image)}  className="card-img-top" alt="..." />
                            <div className="card-body">
                            <h5 className="card-title">Title: {movie.title}</h5>
                            <p className="card-text">Description: {movie.description}</p>
                            <p className="card-text"><small className="text-muted">Year: {movie.year}</small></p>
                            </div>
                        </div>
                        </div>
                    </div>
                        )
                    }
                })
            }

        {/* VideoPlayer */}
        <div className="flowplayer-embed-container mb-5 mt-5" style={{position: 'relative', paddingBottom: '56.25%', height: 0, overflow: 'hidden', maxWidth: '100%'}}>
        <iframe style={{position: 'absolute', top: 0, left: 0, width: '100%', height: '100%'}} webkitallowfullscreen mozallowfullscreen allowFullScreen src="//ljsp.lwcdn.com/api/video/embed.jsp?id=3afc64ea-fe85-456c-b9d7-6d1de8350ff2&pi=cad9d975-ccae-4757-88a3-a65ebb7419f8" title={0} byline={0} portrait={0} width={640} height={360} frameBorder={0} allow="autoplay">
        </iframe>
         </div>
         
         {/* Comments */}
         <div className="form-floating" style={{marginBottom:'190px'}}>
        <textarea className="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style={{height: '100px'}} defaultValue={""} />
        <label htmlFor="floatingTextarea2">Comments</label>
         </div>

    </div>
    )
}

export default Singlemovie
