import React, { useState, useEffect } from "react";
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Main from './Menu/Main';
import Error from './Menu/Error'
import Create from './Menu/Create';
import Registration from './Auth/Registration';
import Login from './Auth/Login';
import Category from './Menu/Category';
import Singlemovie from './Menu/Singlemovie'
import Axios from 'axios';
import Navigation from './Menu/Navigation';
import Search from './Menu/Search';
import Cookies from 'js-cookie'

function Routercomponent() {

  const [loginData, setLoginData] = useState( //ამოწმებს token ს თუ არის Cook ში მაშინ ინახავს ამ ცვლადში, თუარადა ნალს უტოლებს
    Cookies.get("token") ? Cookies.get("token") : null
  );
  const [movies,setMovies]=useState([]);
  const [searchMovies, setSearchMovies] = useState([]);
  const [categorys, setCategorys] = useState([]);
  const [error, setError] = useState([]);
  useEffect( async ()=>{
      await Axios.get('http://127.0.0.1:8000/api/movies').then((res)=>{
          setMovies(res.data);
      }).catch((error)=>{
          setError('Server error');
      });

     await Axios.get('http://127.0.0.1:8000/api/categorys').then((res)=>{
          setCategorys(res.data);
      }).catch((error)=>{
          setError('Server error');
      });
  },[]);

  const searchFunction = (search)=>{
      if(search != '')
      {
        let newMovies = movies.filter((movie)=>{
          if(movie.title.toLowerCase().includes(search.toLowerCase()))
          {
            return movie;
          }
        })
        setSearchMovies(newMovies);
      }else{
        setSearchMovies([]);
      }
  }

  const logOut = ()=>{
    Cookies.remove("token");
    setLoginData(null);
  }
    return (
        <Router>
        <div className='container'>
        <Navigation loginData={loginData} categorys={categorys} searchFunction={searchFunction} logOut={logOut} />
        </div>
        <Routes>
             <Route path='/' element={<Main movies={movies}/>} />
             <Route path='/create' element={<Create/>} />
             {loginData ? <Route path='/registration' element={<Main movies={movies}/>}/> : <Route path='/registration' element={<Registration/>}/> }
             {loginData ? <Route path='/login' element={<Main movies={movies}/>} /> : <Route path='/login' element={<Login/>} />}
             <Route path='serien' element={<Category categoryName='serien'/>} />
             <Route path='/singlemovie/:id' element={<Singlemovie movies={movies}/>} />
             <Route path='/search' element={<Search searchMovies={searchMovies} />}/>
             {categorys.map((category)=>{
               return <Route key={category.id} path={'/'+category.category_name} element={<Category categoryName={category.category_name} movies={movies}/>} />
             })}
             <Route path='*' element={<Error/>}/>
        </Routes>
      </Router>
  
    )
}

ReactDOM.render(<Routercomponent />, document.getElementById('main'));
