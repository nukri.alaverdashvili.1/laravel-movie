<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')
        ->insert(array(
            0=>array(
                'category_name'=>'action',
            ),
            1=>array(
                'category_name'=>'drama',
            ),
            2=>array(
                'category_name'=>'horror',
            ),
            3=>array(
                'category_name'=>'comedy',
            ),
            4=>array(
                'category_name'=>'adventure',
            ),
            5=>array(
                'category_name'=>'tv_shows',
            )
        ));
    }
}
